package li;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
       Scanner scanner = new Scanner(System.in);
        float salaryPerHours;
        do {
            System.out.println("\nAn employee cannot earn less than $8 per hour");
            System.out.println("Enter the salary per hours");
            salaryPerHours = scanner.nextFloat();
        } while (salaryPerHours < 8);

        float workingHours;
        do {
            System.out.println("\nAn employee cannot work more than 60 hours a week");
            System.out.println("Enter the number of working hours");
            workingHours = scanner.nextFloat();
        } while (workingHours > 60 || workingHours < 0);

        System.out.println("\nSalary per week: $" + method(salaryPerHours, workingHours));
    }

    private static float method (float salaryPerHours, float workingHours) {
        float salaryPerWeek;
        if (workingHours > 40) {
            workingHours -= 40;
            salaryPerWeek = 40 * salaryPerHours + workingHours * 1.5f * salaryPerHours;
        } else {
            salaryPerWeek = workingHours * salaryPerHours;
        }
        return salaryPerWeek;
    }
}
