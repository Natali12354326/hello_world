package se;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class Main {
    public static void main(String[] args) {
        int fileRandom, symbolRandom, dirRandom = (int) (Math.random() * 3 + 1);
        String dirName = "C:\\Users\\Olga\\Desktop\\SE2020_LESSON9";
        File dir;
        OutputStream output;
        for (int i = 0; i <= dirRandom; i++) {
            if (i != 0) {
                dirName = dirName.concat("/Directory_".concat(Integer.toString(i)));
            }
            dir = new File(dirName);
            dir.mkdir();
            fileRandom = (int) (Math.random() * 3 + 1);
            for (int j = 1; j <= fileRandom; j++) {
                try {
                    output = new FileOutputStream(dirName.concat("/File_".concat(Integer.toString(j).concat(".txt"))));
                    symbolRandom = (int) (Math.random() * 191 + 10);
                    for (int i1 = 1; i1 <= symbolRandom; i1++) {
                        output.write(randomNumeral());
                        output.write(' ');
                    }
                    output.close();
                } catch (IOException e) {
                    System.out.println("Ошибка");
                }
            }
        }
    }

                    private static char randomNumeral () {
                        int randomNumeral = (int) (Math.random() * 10);
                        switch (randomNumeral) {
                            case 0:
                                return '0';
                            case 1:
                                return '1';
                            case 2:
                                return '2';
                            case 3:
                                return '3';
                            case 4:
                                return '4';
                            case 5:
                                return '5';
                            case 6:
                                return '6';
                            case 7:
                                return '7';
                            case 8:
                                return '8';
                            case 9:
                                return '9';
                            default:
                                return ' ';
                        }
                    }
}